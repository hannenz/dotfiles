#!/bin/bash

# Basics / Apt stuff
#sudo apt update
#sudo apt install git build-essential tty-clock netcat-openbsd gawk curl wget xclip libxt-dev gnome-tweaks gnome-shell-extension-manager \
	#htop btop libncurses-dev nodejs python3-dev zsh trash-cli jq plocate sxiv mpv
#sudo apt install powertop

cd $HOME

mkdir -p $HOME/.local/bin


# Getoptions
wget https://github.com/ko1nksm/getoptions/releases/latest/download/getoptions -O $HOME/.local/bin/getoptions 
chmod +x $HOME/.local/bin/getoptions
wget https://github.com/ko1nksm/getoptions/releases/latest/download/gengetoptions -O $HOME/.local/bin/gengetoptions
chmod +x $HOME/.local/bin/gengetoptions



# Kitty 
curl -L https://sw.kovidgoyal.net/kitty/installer.sh | sh /dev/stdin 

# Better Kitty Icons
KITTY_DIR=$HOME/.local/kitty.app
KITTY_ICON_DIR=$HOME/Downloads/kitty-icon
rm -rf $KITTY_ICON_DIR
git clone https://github.com/k0nserv/kitty-icon.git $KITTY_ICON_DIR
cp $KITTY_DIR/lib/kitty/logo/kitty.png{,orig} 
cp $KITTY_DIR/lib/kitty/logo/kitty-128.png{,.orig}
cp $KITTY_ICON_DIR/build/neue_outrun.iconset/icon_32x32.png $KITTY_DIR/lib/kitty/logo/kitty.png
cp $KITTY_ICON_DIR/build/neue_outrun.iconset/icon_128x128.png $KITTY_DIR/lib/kitty/logo/kitty-128.png
sed -i "s/Icon=kitty/Icon=${KITTY_DIR}\/lib\/kitty\/logo\/kitty.png/" $KITTY_DIR/share/applications/kitty.desktop
# Create symbolic links to add kitty and kitten to PATH (assuming ~/.local/bin is in your system-wide PATH)
ln -sf $KITTY_DIR/bin/kitty $KITTY_DIR/bin/kitten ~/.local/bin/
# Place the kitty.desktop file somewhere it can be found by the OS
cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
# If you want to open text files and images in kitty via your file manager also add the kitty-open.desktop file
cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
# Update the paths to the kitty and its icon in the kitty desktop file(s)
sed -i "s|Icon=kitty|Icon=$(readlink -f ~)/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
sed -i "s|Exec=kitty|Exec=$(readlink -f ~)/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
# Make xdg-terminal-exec (and hence desktop environments that support it use kitty)
echo 'kitty.desktop' > ~/.config/xdg-terminals.list

# Kitty grab
cd ~/.config/kitty
git clone https://github.com/yurikhan/kitty_grab.git

# NVM
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
nvm install 20.14
nvm alias default 20.14



# Vim
git clone https://github.com/vim/vim ~/Downloads/vim
cd ~/Downloads/vim/src
./configure --with-features=huge --enable-terminal --enable-multibyte --enable-gui --enable-cscope --enable-python3interp --with-python3-config-dir= --enable-perlinterp=dyn --with-compiledby=Johannes Braun <hannenz@posteo.de>
make -j8
sudo make install
cd $HOME



# Iosevka Term Nerd Font
wget -O ~/Downloads/IosevkaTerm.zip https://github.com/ryanoasis/nerd-fonts/releases/download/v3.3.0/IosevkaTerm.zip
mkdir -p ~/.local/share/fonts
unzip ~/Downloads/IosevkaTerm.zip -d ~/.local/share/fonts/
fc-cache -rf



# Atuin
curl --proto '=https' --tlsv1.2 -LsSf https://setup.atuin.sh | sh



# chatgpt cli
curl -sS https://raw.githubusercontent.com/0xacx/chatGPT-shell-cli/main/install.sh | sudo -E bash
# Remember to have ~/.openai.token

# Flatpaks
# [✓] Zen
# [ ] whatsapp
# [ ] Bitwarden
# [✓] Authenticator
# [✓] Inkscape
# [ ] Gimp
# [ ] Thunderbird
# [ ] Ressources

# Nice to have's
# [ ] Lazygit
# [ ] Lazydocker
# [ ] Neovim

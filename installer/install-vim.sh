#!/bin/bash
# Vim

# Prerequisites on Fedora:
# `dnf install python3-devel` or `apt install python3-config make automake gcc gcc-c++ kernel-devel ncurses-devel`

test -e ~/Downloads/vim || git clone https://github.com/vim/vim ~/Downloads/vim

cd ~/Downloads/vim/src
./configure \
	--with-features=huge \
	--with-x \
	--enable-terminal \
	--enable-multibyte \
	--enable-gui \
	--enable-cscope \
	--enable-python3interp=yes \
	--with-python3-config-dir="$(python3-config --configdir)" \
	--enable-perlinterp=dyn \
	--with-compiledby="Johannes Braun <hannenz@posteo.de>"

make -j 8 reconfig
sudo make install

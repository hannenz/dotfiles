#!/bin/bash
# Iosevka Term Nerd Font
wget -O ~/Downloads/IosevkaTerm.zip https://github.com/ryanoasis/nerd-fonts/releases/download/v3.3.0/IosevkaTerm.zip
mkdir -p ~/.local/share/fonts
unzip ~/Downloads/IosevkaTerm.zip -d ~/.local/share/fonts/
fc-cache -rf


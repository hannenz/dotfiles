#!/bin/bash



source /etc/os-release
if [[ $NAME == 'Fedora' ]] ; then
	# Fedora
	sudo dnf install bat
elif [[ $NAME == 'Ubuntu' ]] || [[ $NAME == 'Zorin' ]] ; then
	# Ubuntu-/Debian-based
	sudo apt-get install batcat
fi


mkdir -p "$(bat --config-dir)/themes"
wget -P "$(bat --config-dir)/themes" https://github.com/catppuccin/bat/raw/main/themes/Catppuccin%20Latte.tmTheme
wget -P "$(bat --config-dir)/themes" https://github.com/catppuccin/bat/raw/main/themes/Catppuccin%20Frappe.tmTheme
wget -P "$(bat --config-dir)/themes" https://github.com/catppuccin/bat/raw/main/themes/Catppuccin%20Macchiato.tmTheme
wget -P "$(bat --config-dir)/themes" https://github.com/catppuccin/bat/raw/main/themes/Catppuccin%20Mocha.tmTheme
bat cache --build
export BAT_THEME="Catppuccin Frappe"

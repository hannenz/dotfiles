#!/bin/bash
wget -O  $HOME/Downloads/yazi-x86_64-unknown-linux-musl.zip https://github.com/sxyazi/yazi/releases/download/v0.4.2/yazi-x86_64-unknown-linux-musl.zip
unzip $HOME/Downloads/yazi-x86_64-unknown-linux-musl.zip -d $HOME/.local/bin/
ln -s $HOME/.local/bin/yazi-x86_64-unknown-linux-musl/yazi $HOME/.local/bin/yazi

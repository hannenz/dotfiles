#!/bin/sh
# date=2022-06-20
if [ $(khal list --format "{location}" today today | grep -ciE 'johannes|alle') -gt 0 ] ; then

	printf "HALMA the Meeting company presents:\n\n"
	khal --color list --format "{bold}{start-time} {green}{tab}{title}{reset}{nl}{tab}{location}{nl}{tab}{calendar}{nl}" today today | egrep -i 'johannes|alle' -B 2 -A 2

else
	echo "Keine Termine heute"
fi

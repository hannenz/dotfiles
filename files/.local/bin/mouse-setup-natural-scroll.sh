#!/bin/sh
xinput list --name-only | grep -i mouse | while read device ; do
	xinput --set-prop "$device" "libinput Natural Scrolling Enabled" 1
done

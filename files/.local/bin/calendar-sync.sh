#!/bin/sh
# Synchronize CalDav calendars
# @author Johannes Braun
# @version 2022-06-23

# Needed for dunstify notifications
export DISPLAY=:0
export XDG_RUNTIME_DIR=/run/user/$(id -u)

# Use host specific configuration
HOSTNAME=$(cat /etc/hostname)
export VDIRSYNCER_CONFIG="/${HOME}/.config/vdirsyncer/config.${HOSTNAME}"


printf "\n\n%s: Start synchronizing\n" "$(date)"
if /usr/bin/vdirsyncer sync 2>&1 > /dev/null ; then
	printf "%s: Calendar synchronisation was successful\n" "$(date)"
else
	printf "%s: sCalendar synchronisation failed\n" "$(date)"
	/usr/bin/dunstify --urgency 2 "❌ Calendar synchronisation failed"
fi

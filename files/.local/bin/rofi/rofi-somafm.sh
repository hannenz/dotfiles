#!/bin/bash
#
# Rofi menu to select a SomaFM channel
#
# Depends on [somafm-cli](https://github.com/rockymadden/somafm-cli)
# 
# The custom alacritty sticky window as inspired by
# https://github.com/tom-on-the-internet/sticky-notes 
# Not really that useful but some proof-of-concept.
# TO be used with:
#
# - ~/.config/alacritty/somafm.yml
# - ~/.config/zsh/.zshrc
# - ~/.config/bspwm/bspwmrc
#
# Johannes Braun <hannenz@posteo.de>
# 2022-06-09
#
export SOMAFM="${HOME}/.local/bin/somafm"
config_file="/home/jbraun/.config/alacritty/somafm.yml"

channels=$(${SOMAFM} list | awk -F\| '{print $1 }')
channel=$( echo "${channels}" | rofi -dmenu -p "SomaFM: Select Channel")
if [ ! -z "${channel}" ] ; then
	export SOMAFM_CHANNEL=${channel}
	alacritty #-e ${SOMAFM} listen "${channel}"
fi

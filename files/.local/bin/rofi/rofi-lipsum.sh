#!/bin/sh

# trim newlines from lipsum's output (which is inconsisstent unfortunately ...)
function lip {
	lipsum $* | tr -d '\n'
}

text="Generate"
while  [[ $text =~ "Generate" ]] ; do
	menu=$(printf "󰑐 Generate again\n%s\n%s\n%s" "$(lip -w 1)" "$(lip -s 1)" "$(lip -p 1)")
	text=$(echo "${menu}" |rofi -dmenu -p "Placeholder text")
done
echo $text | tee >(xclip -se clipboard -i)
notify-send "Copied to clipboard" "$text"

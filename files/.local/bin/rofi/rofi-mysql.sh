#!/bin/sh
MYSQL_CMD='mariadb -P 4100 -h 127.0.0.1 -u root -ppebble'
db=$(echo "SHOW DATABASES" | ${MYSQL_CMD} | rofi -dmenu)
if [ ! -z "$db" ] ; then
	st -e ${MYSQL_CMD} "${db}"
fi

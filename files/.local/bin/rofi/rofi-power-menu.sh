#!/bin/bash
# ---------------------------------------------
# ~/.local/bin/rofi-power-menu.sh
# 2022-06-08 
# Johannes Braun <hannenz@posteo.de>
#
# Rofi menu for system / power menu
# ---------------------------------------------

confirm=0;
lang="de";


case "${lang}" in
	"de")
		read -r -d '' menu <<-'EOF'
			‭Ausschalten
			‭Neu starten
			‭Abmelden
			‭Bildschrim sperren
			‭Abbrechen
EOF
		;;
	"en"|*)
		read -r -d '' menu <<-'EOF'
			‭Power off
			‭Reboot
			‭Logout
			‭Lock screen
			‭Cancel
EOF
		;;
esac


# Rofi options
read -r -d '' options <<-'EOF'
 -dmenu
 -format d
 -i
EOF

# Ask for action
action=$(echo "${menu}" | \
	rofi ${options} \
	-theme-str 'listview { lines: 5; } window { width: 400; location: north east; anchor: north east; y-offset: 42px; x-offset: -16px; } ' \
	-p System \
)
# Cancelled by ESC or Cancel action?
if [ -z "${action}" ] || [ "${action}" = "4" ] ; then 
	exit 0
fi


# Confirm action (optionally/configurable)
if [ "${confirm}" = "1" ] ; then
	answer=$(printf "Ja\nNein" | \
		rofi ${options} \
		-lines 2 \
		-width 16 \
		-p "Sind Sie sicher?" \
	)

	if [ "${answer}" != "1" ] ; then
		exit 0
	fi
fi

# Detect the window manager
wm=$(wmctrl -m | awk '/Name:/{print $2}')

case "${action}" in
	"1") 
		poweroff ;;
	"2") 
		reboot ;;
	"3") 
		killall "${wm}" ;;
	"4") 
		xlock ;;
	*) 
		exit 0 ;;
esac

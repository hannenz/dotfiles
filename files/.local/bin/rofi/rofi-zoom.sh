#!/bin/bash
if [ -z "${ROFI_RETV}" ]
then
	rofi -show zoom -modi "zoom:$0"
else
	case "${ROFI_RETV}" in
		"0")
			read -r -d '' confs <<-'EOF'
				Jobbesprechung\0info\x1f402205043
				Webmeeting\0info\x1f629697406
				PM-Meeting\0info\x1f917800453
				Yoga\0info\x1f629697406
			EOF
			echo -en "${confs}"
			;;


		"1"|"2")
			notify-send "Joining zoom meeting ${ROFI_INFO}"
			coproc ( xdg-open "zoommtg://zoom.us/join?confno=$ROFI_INFO" >/dev/null 2>&1 )
			;;
	esac
fi
# echo "ROFI_RETV=${ROFI_RETV}"
# if [[ "${ROFI_RETV}" -ne "0" ]] ; then
# fi
    #
	# 402205043:   Jobbesprechung
	# 629697406: 爵 Webmeeting
	# 629697406: 🧘  Yoga

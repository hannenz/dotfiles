#!/bin/bash
# ---------------------------------------------
# ~/.local/bin/rofi-powersave.sh
# 2022-06-08 
# Johannes Braun <hannenz@posteo.de>
#
# Rofi menu to activate powersave mode
# ---------------------------------------------
lang="de";

case "${lang}" in
	"de")
		read -r -d '' menu <<-'EOF'
			Stromsparmodus aktivieren
			‭ﰸ Abbrechen
EOF
		;;
	"en"|*)
		read -r -d '' menu <<-'EOF'
			Enable powersave mode
			‭ﰸ Cancel
EOF
		;;
esac


# Rofi options
read -r -d '' options <<-'EOF'
 -dmenu
 -format d
 -i
EOF

# Ask for action
action=$(echo "${menu}" | \
	rofi ${options} \
	-lines 2 \
	-width 20 \
	-p System \
)

if [ "${action}" = "1" ] ; then
fi

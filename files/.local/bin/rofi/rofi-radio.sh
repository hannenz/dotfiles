#!/bin/bash
if [[ ! $ROFI_RETV ]] ; then
	rofi -show Radio -modi "Radio:/home/hannenz/.local/bin/rofi/rofi-radio.sh" -no-cycle -theme nord
	exit 0
fi

if [[ $ROFI_RETV -eq 0 ]] ; then 
	curl -s -H 'Accept: application/json' https://somafm.com/channels.json |\
		jq -r '.channels | sort_by(.title) | .[]' |\
		jq -r '.title + "|" + .description + "|" + .id' |\
		while read channel ; do
			title=$(echo $channel | cut -d'|' -f 1)
			description=$(echo $channel | cut -d'|' -f 2)
			id=$(echo $channel | cut -d'|' -f 3)
			echo -en "${title}: ${description}\0info\x1f${id}\n"
		done 
fi

if [[ ! -z "${ROFI_INFO}" ]] ; then
	notify-send ${ROFI_INFO}
	coproc ( ${HOME}/.local/bin/somafm listen ${ROFI_INFO} > /dev/null 2>&1 )
fi

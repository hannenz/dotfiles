#!/bin/bash
destdir="${HOME}/Bilder/Bildschirmfotos"

# u202d: LTR mode, since some fontawesome(?) icons will force RTL mode 
read -r -d '' menu <<-'EOF'
	‭麗 Select window or custom region
	‭类 Currently focussed window
	‭ Whole screen
	‭ﰸ Cancel
EOF

# Rofi options
# -dmenu		# dmenu mode
# -format d		# Return the chosen index instead of the string
# -i 			# Case insensitive
# -p System		# Prompt text
# -lines 4 		# Height
# -width 20		# Width
# -theme nord	# Theme
# rofi_options='-dmenu -format d -i -p "Take a screenshot" -lines 4 -width 32 -theme nord'
read -d '' options <<-'EOF'
	-dmenu
	-format d
	-i
	-p \"Screenshot\ Lorm\"
	-lines 4
	-width 32
EOF


chosen=$(echo "${menu}" | rofi -dmenu -format d -i -p "Take a screenshot, save it to disk and copy to clipboard " -lines 4 -width 32)
case "$chosen" in
	"1") 
		mode="--select"
		;;
	"2") 
		mode="--focused"
		;;
	"3") 
		mode="--autoselect"
		;;
	*) 
		exit 0
		;;
esac
file=$(scrot $mode --exec 'ls $f') && xclip -selection clipboard -t image/png -i "${file}" && sxiv "${file}"
dunstify "${file}"

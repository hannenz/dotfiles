#!/bin/sh
# These tasks needs to be done every time the monitor/screen setup changes, i.e.
# docking/undocking the laptop etc.
${HOME}/.fehbg
setxkbmap -layout us -variant altgr-intl -option ctrl:nocaps
xset r rate 300 50
mouse-setup-natural-scroll.sh

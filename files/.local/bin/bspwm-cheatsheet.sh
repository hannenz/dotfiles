#!/bin/bash

# Hide cursor
echo -en "\033[?25l"
# Cursor Home
echo -en "\033[?H"

text=$(grep -ve '^\s*#' ~/.config/sxhkd/sxhkdrc)
# text=$(echo "$text" | grep -ve '^\s*$')
# text=$(echo "$text" | head -n 20);

export BAT_THEME="Nord"
bat --pager "less -Ri" --style=plain --language=sh --theme=Nord ~/.config/sxhkd/sxhkdrc
# less --ignore-case  ~/.config/sxhkd/sxhkdrc
# echo "$text" | less

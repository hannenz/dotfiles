#!/bin/sh
chosen=$(printf "Power Off\nRestart\nLock" | rofi -dmenu -i)
# Exit on abort / <esc>
test -z "${chosen}" && exit 0;

# Confirm
answer=$(printf "Yes\nNo" | rofi -dmenu -i -p "${chosen}")
if [ "${answer}" = "Yes" ] ; then
	case "${chosen}" in
		"Power off") poweroff ;;
		"Restart") reboot ;;
		"Lock") bspc quit ;;
		*) exit 1 ;;
	esac
fi

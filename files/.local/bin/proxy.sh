#!/bin/bash
proxy_host=$(gsettings get org.gnome.system.proxy.http host | sed "s/^'\|'$//g")
proxy_port=$(gsettings get org.gnome.system.proxy.http port | sed "s/^'\|'$//g")
no_proxy=$(gsettings get org.gnome.system.proxy ignore-hosts | sed "s/^\[\|'\|\]$//g")

function proxy() {


	case $1 in
		"on")
			# shift
			# export no_proxy="localhost,127.0.0.1,localaddress,.localdomain.com, swu.dom"
			# proxy="172.22.11.69:8080"
			# if [[ -n $1 ]] ; then
			# 	proxy_host=$1
			# 	shift
			# 	proxy_port=$2
			# 	shift
			# fi

			# if [[ $proxy_host =~ '([0-9]{1,3}.?){4}:([0-9]+)' ]] ; then
			# 	echo "Proxy: $proxy_host:$proxy_port"
			# else
			# 	echo "Invalid address: $proxy_host" >&2
			# 	exit 1
			# fi
			export  no_proxy=$no_proxy \
					NO_PROXY=$no_proxy \
					http_proxy=http://$proxy_host:$proxy_port \
					HTTP_PROXY=http://$proxy_host:$proxy_port \
					https_proxy=http://$proxy_host:$proxy_port \
					HTTPS_PROXY=http://$proxy_host:$proxy_port \
					ftp_proxy=$proxy_host:$proxy_port \
					FTP_PROXY=$proxy_host:$proxy_port \
					rsync_proxy=$proxy_host:$proxy_port \
					RSYNC_PROXY=$proxy_host:$proxy_port
					all_proxy=http://$proxy_host:$proxy_port \
					ALL_PROXY=http://$proxy_host:$proxy_port
					return 0
		;;

	"off")
		unset http_proxy \
			https_proxy \
			ftp_proxy \
			rsync_proxy \
			HTTP_PROXY \
			HTTPS_PROXY \
			FTP_PROXY \
			RSYNC_PROXY \
			ALL_PROXY \
			all_proxy
		;;

	*)
		if [ -n "$http_proxy" ] ; then
			env | grep -i proxy
			# printf "http_proxy:   %s\n" $http_proxy
			# printf "https_proxy:  %s\n" $https_proxy
			# printf "ftp_proxy:    %s\n" $ftp_proxy
			# printf "rsync_proxy:  %s\n" $rsync_proxy
			# printf "no_proxy:     %s\n" $no_proxy
		fi
		;;
	esac
}



function autoproxy()
{
	cache_file=/tmp/proxy.cache

	if [ -e ${cache_file} ] ; then
		# echo "Reading proxy config from cache (${cache_file})"
		source ${cache_file}
	else
		# Ping is not suitable for testing proxy, since ICMP might 
		# not be handled by the proxy, we better use netcat's "zap" 
		# testing
		if ! which netcat 2>/dev/null 2>&1; then
			echo "netcat is not installed."
			return
		fi

		if netcat -w 2 -z $proxy_host $proxy_port ; then
			proxy on "http://$proxy_host:$proxy_port" 2>&1 >/dev/null
			# echo "Writing proxy config to cache (${cache_file})"
			env | grep -i _PROXY | awk -F= '{ print $1 "=\"" $2 "\"" }'> ${cache_file}
		fi
	fi
}

#!/bin/bash
# Checks if .htaccess redirection  works as extended in that it curls the given
# URLs and outputs the effective URL that will be called after all redirects
# have been performed (should be the same for all input urls)
#
# A working example of .htaccess rules is:
#
# # Catch-All
# RewriteCond %{HTTP_HOST} ^(www\.)?schweinneun.de$ [NC]
# RewriteRule ^(.*)$ https://www.schwein9.de [R=301,L]
# 
# # Force www
# RewriteCond %{HTTP_HOST} ^schwein9\.de$ [NC]
# RewriteRule ^(.*)$ https://www.%{HTTP_HOST}/$1 [R=301,L]
# 
# #Force HTTPS
# RewriteCond %{HTTPS} off
# RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301]
#
# This will
# - catch all domains and redirect to a canonical one, in this case schwein9.de
# is the canonical domain to be used and schweinneun.de shall be redirected
# - redirect non-www to www
# - redirect HTTP to HTTPS
# 
# So the desired output is `https://www.schwein9.de` for all input urls
#
for url in "${@}" ; do
	redirectedUrl=$(curl -sL "${url}" -o /dev/null -w '%{url_effective}')
	printf "%-40s -> %s\n" $url $redirectedUrl
done

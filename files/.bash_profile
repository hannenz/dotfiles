#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc
export PATH=$PATH
. "$HOME/.cargo/env"

. "$HOME/.atuin/bin/env"

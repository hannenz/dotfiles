# vim:ft=dircolors
# (This is not a dircolors file but it helps to highlight colors and comments)

# default values from dircolors
# (entries with a leading # are not implemented in lf)
# #no     00              # NORMAL
# fi      00              # FILE
# #rs     0               # RESET
# di      01;34           # DIR
# ln      01;36           # LINK
# #mh     00              # MULTIHARDLINK
# pi      40;33           # FIFO
# so      01;35           # SOCK
# #do     01;35           # DOOR
# bd      40;33;01        # BLK
# cd      40;33;01        # CHR
# or      40;31;01        # ORPHAN
# #mi     00              # MISSING
# su      37;41           # SETUID
# sg      30;43           # SETGID
# #ca     30;41           # CAPABILITY
# tw      30;42           # STICKY_OTHER_WRITABLE
# ow      34;42           # OTHER_WRITABLE
# st      37;44           # STICKY
# ex      01;32           # EXEC

# default values from lf (with matching order)
# ln      01;36   # LINK
# or      31;01   # ORPHAN
# tw      01;34   # STICKY_OTHER_WRITABLE
# ow      01;34   # OTHER_WRITABLE
# st      01;34   # STICKY
# di      01;34   # DIR
# pi      33      # FIFO
# so      01;35   # SOCK
# bd      33;01   # BLK
# cd      33;01   # CHR
# su      01;32   # SETUID
# sg      01;32   # SETGID
# ex      01;32   # EXEC
# fi      00      # FILE

# file types (with matching order)
ln      03;01;38;5;116   # LINK (cyan)
or      38;5;203      # ORPHAN (red)
tw      38;5;109      # STICKY_OTHER_WRITABLE (light blue)
ow      38;5;109      # OTHER_WRITABLE (light blue)
st      01;38;5;110   # STICKY (blue)
di      01;38;5;111   # DIR (blue)
pi      38;5;175      # FIFO (purple)
so      01;38;5;203   # SOCK (red)
bd      38;5;175;01   # BLK (purple)
cd      38;5;175;01   # CHR (purple)
su      01;38;5;116   # SETUID (light cyan)
sg      01;38;5;116   # SETGID (light cyan)
ex      01;38;5;29   # EXEC (orange)
fi      38;5;252      # FILE (light gray)

# archives or compressed
*.tar   38;5;168
*.tgz   38;5;168
*.zip   38;5;168
*.gz    38;5;168
*.bz2   38;5;168
*.xz    38;5;168
*.rar   38;5;168
*.7z    38;5;168

# image formats
*.jpg   01;38;5;135
*.jpeg  01;38;5;135
*.gif   01;38;5;135
*.bmp   01;38;5;135
*.png   01;38;5;135
*.svg   01;38;5;135
*.ico   03;01;38;5;135

# audio formats
*.mp3   01;38;5;120
*.wav   01;38;5;120
*.flac  01;38;5;120
*.ogg   01;38;5;120

# video formats
*.mp4   38;5;167
*.mkv   38;5;167
*.avi   38;5;167
*.mov   38;5;167

# documents
*.pdf   01;38;5;160
*.doc   01;38;5;160
*.docx  01;38;5;160
*.xls   01;38;5;160
*.xlsx  01;38;5;160
*.ppt   01;38;5;160
*.pptx  01;38;5;160

# source code
*.c     01;38;5;69
*.cpp     01;38;5;69
*.py     01;38;5;69
*.js     38;5;69
*.java     01;38;5;69
*.jar     01;38;5;69
*.sh     01;38;5;69
*.rb     01;38;5;69
*.html     01;38;5;69
*.css     03;01;38;5;69
*.php     01;38;5;69

# text documents
*.md    01;38;5;185
*.txt   01;38;5;185
*.note  01;38;5;185
*.log   03;01;38;5;185

# git related stuff
*.gitignore 03;38;5;131
*.gitmodules 03;38;5;131
*.gitattributes 03;38;5;131

# docker stuff
*.yaml 01;38;5;63
*.yml 01;38;5;63
*.env 01;38;5;63

# config stuff
*.json 01;38;5;161
*.toml 01;38;5;161
*.lock 01;38;5;161
*.xml 01;38;5;161

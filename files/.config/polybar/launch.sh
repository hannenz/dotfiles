#!/usr/bin/env bash
polybar_config_file="~/.config/polybar/themes/light/config.ini"

# Terminate already running bar instances
killall --quiet --wait --signal 2 polybar

# https://github.com/polybar/polybar/issues/763
if type "xrandr" ; then
	for monitor in $(xrandr --query | grep "\bconnected\b" | cut -d" " -f 1); do
		MONITOR=$monitor polybar --config=$polybar_config_file --reload main &
	done
else
	polybar --config=$polybar_config_file --reload main & disown
fi

;=========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]
; background = #66000000
background = #702e3440
nord0 = #2e3440
nord1 = #3b4252
nord2 = #434c5e
nord3 = #4c566a
nord4 = #d8dee9
nord5 = #e5e9f0
nord6 = #eceff4
nord7 = #8fbcbb 
nord8 = #88c0d0
nord9 = #81a1c1
nord10 = #5e81ac
nord11 = #bf616a
nord12 = #d08770
nord13 = #ebcb8b
nord14 = #a3be8c
nord15 = #b48ead
background-alt = #00373B41
foreground = #C5C8C6
primary = #F0C674
secondary = #8ABEB7
alert = #A54242
disabled = #707880
black = #303030

[bar/example]
locale = de_DE.UTF-8
width = 100%
height = 22pt
radius = 0

background = ${colors.background}
foreground = ${colors.nord6}

line-size = 6pt

border-size = 0
border-color = #00000000

padding-left = 0
padding-right = 1

module-margin = 2

; separator = |
; separator-foreground = ${colors.disabled}

;font-0 = Iosevka Nerd Font:size=10;2
font-0 = Inter:size=10;2
font-1 = Inter:size=10;2


modules-left = xworkspaces xwindow
modules-center = date
modules-right = pulseaudio memory cpu wlan eth

cursor-click = pointer
cursor-scroll = ns-resize

enable-ipc = true

tray-position = right

wm-restack = bspwm



[module/xworkspaces]
type = internal/xworkspaces

label-active = %name%
label-active-background = ${colors.nord8}
label-active-foreground = ${colors.black}
label-active-padding = 3

label-occupied = %name%
label-occupied-padding = 3
label-occupied-background = ${colors.nord3}

label-urgent = %name%
label-urgent-background = ${colors.nord12}
label-urgent-padding = 1

label-empty = %name%
label-empty-foreground = ${colors.nord4}
label-empty-padding = 3
label-active-font = 1
font = 1

[module/xwindow]
type = internal/xwindow
label = %title:0:60:...%
font = 1
label-background = ${colors.nord14}
label-foreground = ${colors.nord0}
label-padding = 3


[module/kdeconnect]
type = custom/script
exec = "/usr/local/bin/polybar-kdeconnect.sh -d"
tail = true


[module/filesystem]
type = internal/fs
interval = 25

mount-0 = /

label-mounted =  %{F#F0C674}%mountpoint%%{F-} %percentage_used%%

label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.nord4}

[module/pulseaudio]
type = internal/pulseaudio

format-volume-prefix = "♬"
format-volume-prefix-foreground = ${colors.nord13}
format-volume = <label-volume>

label-volume = %percentage%%
label-volume-font = 1

label-muted = muted
label-muted-foreground = ${colors.nord4}

click-right = pavucontrol

[module/xkeyboard]
type = internal/xkeyboard
blacklist-0 = num lock

label-layout = %layout%
label-layout-foreground = ${colors.nord13}

label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-foreground = ${colors.nord0}
label-indicator-background = ${colors.nord12}

[module/memory]
type = internal/memory
interval = 2
format-prefix = "RAM "
format-prefix-foreground = ${colors.nord13}
label = %percentage_used:2%%

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = "CPU "
format-prefix-foreground = ${colors.nord13}
label = %percentage:2%%

[network-base]
type = internal/network
interval = 5
format-connected = <label-connected>
format-disconnected = <label-disconnected>
label-disconnected = %{F#F0C674}%ifname%%{F#707880} disconnected

[module/wlan]
inherit = network-base
interface-type = wireless
label-connected = %{F#F0C674}%ifname%%{F-} %essid% %local_ip%

[module/eth]
inherit = network-base
interface-type = wired
label-connected = %local_ip%

[module/date]
type = internal/date
interval = 2

date = %H:%M Uhr
date-alt = KW%W: %a, %d. %B %Y %H:%M:%S Uhr

label =   %date%
label-foreground = ${colors.nord6}
label-font = 2

[settings]
screenchange-reload = true
pseudo-transparency = true

; vim:ft=dosini

# --------------------------------
# /home/hannenz/.config/zsh/.zshrc
# 2024-08-11
# <hannenz@posteo.de>
# --------------------------------

# Init / install ZINIT
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"
# Download Zinit, if it's not there yet
if [ ! -d "$ZINIT_HOME" ] ; then
	mkdir -p "$(dirname $ZINIT_HOME)"
	git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi
# Source / Load zinit
source "${ZINIT_HOME}/zinit.zsh"

# Add Powerlevel10k
zinit ice depth=1; zinit light romkatv/powerlevel10k

# Add zsh plugins
zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions

zinit snippet OMZP::command-not-found
# zinit snippet OMZP::docker


fpath=($HOME/.docker/completions $fpath /usr/local/share/zsh/site-functions /usr/share/zsh/vendor-functions /usr/share/zsh/vendor-completions /usr/share/zsh/functions/Calendar /usr/share/zsh/functions/Chpwd /usr/share/zsh/functions/Completion /usr/share/zsh/functions/Completion/AIX /usr/share/zsh/functions/Completion/BSD /usr/share/zsh/functions/Completion/Base /usr/share/zsh/functions/Completion/Cygwin /usr/share/zsh/functions/Completion/Darwin /usr/share/zsh/functions/Completion/Debian /usr/share/zsh/functions/Completion/Linux /usr/share/zsh/functions/Completion/Mandriva /usr/share/zsh/functions/Completion/Redhat /usr/share/zsh/functions/Completion/Solaris /usr/share/zsh/functions/Completion/Unix /usr/share/zsh/functions/Completion/X /usr/share/zsh/functions/Completion/Zsh /usr/share/zsh/functions/Completion/openSUSE /usr/share/zsh/functions/Exceptions /usr/share/zsh/functions/MIME /usr/share/zsh/functions/Math /usr/share/zsh/functions/Misc /usr/share/zsh/functions/Newuser /usr/share/zsh/functions/Prompts /usr/share/zsh/functions/TCP /usr/share/zsh/functions/VCS_Info /usr/share/zsh/functions/VCS_Info/Backends /usr/share/zsh/functions/Zftp /usr/share/zsh/functions/Zle /home/braunj/.zsh/pure)
PATH=${PATH}:${HOME}/.local/bin:${HOME}/.local/bin/rofi:${HOME}/.local/bin/nvim-linux64/bin/:${HOME}/.local/bin/watson-venv/bin:${HOME}/.atuin/bin:/opt/nvim-linux64/bin:/usr/local/go/bin


# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=5000
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups
setopt NO_AUTO_REMOVE_SLASH
setopt no_auto_remove_slash
setopt autocd
# Vim key bindings
bindkey -v

bindkey '^y' autosuggest-accept
autoload -U compinit; compinit

########################
#  Completion styling  #
########################

# Case insensitive completions
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
# Colored completions
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complet:__zoxide_z:*' fzf-preview 'ls --color $realpath'

# Edit current command line in vim with ESC v.
export VISUAL=vim
export EDITOR=vim
autoload -Uz edit-command-line
zle -N edit-command-line
bindkey -M vicmd v edit-command-line


# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Shell integrations
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
eval "#(zoxide init --cmd cd zsh)"


# Enable Vi key bindings
bindkey -v

autoload -Uz promptinit
promptinit
prompt bart

autoload -U zcalc
autoload -U web-search

plugins=(zcalc web-search)


setopt auto_cd
setopt correct
cdpath=($HOME/Sites $HOME/Projects $HOME/pidev/projects/pico $HOME/pidev/projects/pi /media/tini/share/cbm)

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Increase key speed
xset r rate 300 50

[ -s "$HOME/.cargo/env" ] && \. "$HOME/.cargo/env"


#############
#  ALIASES  #
#############
source $HOME/.aliases


HOSTNAME=$(</etc/hostname)
function hostname() {
	echo ${HOSTNAME}
}


# export VDIRSYNCER_CONFIG="${HOME}/.config/vdirsyncer/config.${HOSTNAME}"


# Make a sql dump of the given database. Dump is written to /tmp
# function mksqldump () {
#     if [ $# -lt 1 ] ; then
#         echo "usage: mksqldump database [email-address]"
#         return
#     fi
#     dumpfile=/tmp/$1.$(hostname).$(date +%F-%H%M%S).sql.gz
#     mysqldump -u root -p -- $1 | gzip > "${dumpfile}"
#     echo "Dump has been written to ${dumpfile}"
# 	if [ $# -eq 2 ] ; then
# 		thunderbird --compose "to=$2,subject=SQL-Dump `basename ${dumpfile}`,attachment=${dumpfile},format=text"
# 	fi
# }
#
# date/time format for ls
export TIME_STYLE=long-iso

autoload -U history-search
bindkey '^N' history-beginning-search-backward
bindkey '^P' history-beginning-search-forward


if [[ -e ~/.config/openai.token ]] ; then
	export OPENAI_API_KEY=$(cat ~/.config/openai.token)
	export OPENAI_API_TOKEN=${OPENAI_API_KEY}
	export OPENAI_KEY=${OPENAI_API_KEY}
fi

test -e $HOME/.local/bin/bashmarks.sh && source $HOME/.local/bin/bashmarks.sh


case "$HOSTNAME" in
	calvin)
		export PASSWORD_STORE_DIR=$HOME/Nextcloud/password-store/
		export VIMWIKI_PATH="$HOME/Nextcloud/vimwiki"
		export VIMWIKI_PATH_HTML="$HOME/Nextcloud/vimwiki_html"
		;;

	t440s)
		export PASSWORD_STORE_DIR=$HOME/Nextcloud/password-store/
		export VIMWIKI_PATH="$HOME/Nextcloud/vimwiki"
		export VIMWIKI_PATH_HTML="$HOME/Nextcloud/vimwiki_html"
		;;

	GX8ZCY3) 
		function winip {
			ip=$(ipconfig.exe | grep -oE 'IPv4-Adresse.*54' | grep -oE '([0-9]{1,3}\.?){4}')
			echo $ip
		}
		export VIMWIKI_PATH="$HOME/vimwiki"
		export VIMWIKI_PATH_HTML="$HOME/vimwiki_html"
		test -e $HOME/.local/bin/proxy.sh && source $HOME/.local/bin/proxy.sh
		autoproxy
		;;
	56YXV64)
		export VIMWIKI_PATH="$HOME/vimwiki"
		export VIMWIKI_PATH_HTML="$HOME/vimwiki_html"
		test -e $HOME/.local/bin/proxy.sh && source $HOME/.local/bin/proxy.sh
		autoproxy
		;;
	yoga)
		export VIMWIKI_PATH="$HOME/Nextcloud/vimwiki"
		export VIMWIKI_PATH_HTML="$HOME/Nextcloud/vimwiki_html"
		;;
esac


#eval "$(zoxide init zsh)"

eval "$(atuin init zsh --disable-up-arrow)"
function gi() { curl -sLw "\n" https://www.toptal.com/developers/gitignore/api/$@ ;}


################################
#  NVM - Node version manager  #
################################
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
# place this after nvm initialization!
autoload -U add-zsh-hook

load-nvmrc() {
  local nvmrc_path
  nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version
    nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$(nvm version)" ]; then
      nvm use
    fi
  elif [ -n "$(PWD=$OLDPWD nvm_find_nvmrc)" ] && [ "$(nvm version)" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}

add-zsh-hook chpwd load-nvmrc
load-nvmrc



function pet-select() {
  BUFFER=$(pet search --query "$LBUFFER")
  CURSOR=$#BUFFER
  zle redisplay
}
zle -N pet-select
# stty -ixon
bindkey '^s' pet-select

export WUNDERLAMP_PROJECT_DIR=$HOME/Projects
export WUNDERLAMP_HTTP_PORT_PHP8=32876
export WUNDERLAMP_HTTPS_PORT_PHP8=32877
export WUNDERLAMP_HTTP_PORT_PHP7=32776
export WUNDERLAMP_HTTPS_PORT_PHP7=32777
export UID GID

if which bat >/dev/null 2>&1 ; then
	export BAT_THEME="Catppuccin Mocha"
	export MANPAGER="sh -c 'col -bx | bat -l man -p'"
	export MANROFFOPT="-c"
fi

eval "$(direnv hook zsh)"
which caniuse && source <(caniuse --completion)

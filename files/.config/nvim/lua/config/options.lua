-- Options are automatically loaded before lazy.nvim startup
-- Default options that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/options.lua
-- Add any additional options here
vim.g.autoformat = false
vim.g.minianimate_disable = true
vim.g.snacks_animate = false
vim.g.snacks_indent = false
vim.o.shiftwidth = 4
vim.o.tabstop = 4
vim.o.expandtab = false
vim.o.laststatus = 2
vim.o.fixendofline = false
vim.o.relativenumber = false
vim.o.list = false
-- vim.o.clipboard:append("unnamedplus") -- use system clipboard as default register
vim.g.switch_mapping = "sw"
vim.g.lazyvim_mini_snippets_in_completion = true

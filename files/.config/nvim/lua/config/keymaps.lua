-- Keymaps are automatically loaded on the VeryLazy event
-- Default keymaps that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/keymaps.lua
-- Add any additional keymaps here
vim.keymap.del("n", "<S-h>")
vim.keymap.del("n", "<S-l>")

vim.keymap.set("i", "kj", "<Esc>")
vim.keymap.set("i", "jj", "<Esc>")
vim.keymap.set("n", "<leader>m", ":!make<CR>")
vim.keymap.set("n", "<leader>d", "^i[✓] <Esc>df]")
vim.keymap.set("n", "<leader>D", "^i[ ] <Esc>df]")

-- Move visual block
vim.keymap.set("n", "<c-s-j>", ":m .+1<CR>==")
vim.keymap.set("n", "<c-s-k>", ":m .-2<CR>==")
vim.keymap.set("v", "<c-s-j>", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "<c-s-k>", ":m '<-2<CR>gv=gv")

vim.keymap.set("n", "<Leader>v", ":vsplit<CR>")
vim.keymap.set("n", "<Leader>y", ":terminal<CR>")

-- ─< rename word under cursor >───────────────────────────────────────────────────────────
vim.keymap.set("n", "<leader>R", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])


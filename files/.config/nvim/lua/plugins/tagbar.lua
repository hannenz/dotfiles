return {
	{
		"majutsushi/tagbar",
		name = "tagbar",
		cmd = "TagbarToggle",
		keys = function()
			return {
				{ "<leader>t", "<cmd>TagbarToggle<cr>" },
			}
		end,
		opts = {
			tagbar_position = "botright vertical",
			tagbar_autofocus = 1,
			tagbar_compact = 1,
		},
		config = function() end,
	},
}

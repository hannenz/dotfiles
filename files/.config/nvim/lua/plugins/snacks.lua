return 	{
	"snacks.nvim",
	opts = {
		scroll = { enabled = false },
		indent = { enabled = false, },
		chunk = { enabled = false }
	}
}

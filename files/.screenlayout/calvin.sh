#!/bin/sh
xrandr --output DisplayPort-0 --off --output DisplayPort-1 --off --output HDMI-A-0 --off --output HDMI-A-1 --off --output DVI-D-0 --off --output DisplayPort-2 --mode 1920x1080 --pos 0x0 --rotate normal --output DisplayPort-3 --off --output DisplayPort-4 --primary --mode 1920x1080 --pos 1920x0 --rotate normal --output DisplayPort-5 --off

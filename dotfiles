#!/bin/bash

# @author Johannes Braun <hannenz@posteo.de>
# @version 2024-08-11
VERSION="2.0"
PROG=${0##*/}

parser_definition() {
	setup   REST help:usage -- "Usage: $PROG [global options...] [command] [options...] [arguments...]"
	msg -- '' 'Global Options:' ''
	param 	DOTFILES_DIR	-d --dotfiles-dir 	-- 	"Dotfiles directory"
	param 	BACKUP_DIR 		-b --backup-dir 	--  "Backup directory"
	flag 	QUIET			-q --quiet 			-- 	"Suppress unnecessary output"
	disp    :usage  		-h --help
	disp    VERSION			--version

	msg -- '' 'Commands:' ''

	cmd install -- "Install dotfiles to $HOME"
	cmd add     -- "Add file from $HOME to dotfiles (import)"
	msg --
}


parser_definition_install() {
	setup REST help:usage abbr:true -- "Usage: $PROG install [options...]"
	msg -- 'Install Options:'
	flag 	DRY_RUN			-n --dry-run 		-- "Dry run: Do not actually copy files, just print what would be copied"
	flag 	QUIET			-q --quiet 			-- 	"Suppress unnecessary output"
	disp    :usage  		-h --help
}

parser_definition_add() {
	setup REST help:usage abbr:true -- "Usage: $PROG add [options...] file"
	msg -- 'Add Options:'
	disp    :usage  	-h --help
}

function help() {
	printf "\n\n"
	printf "HERE BE THE HELP TEXT\n"
	printf "\n\n"
}

do_add() {
	FILE=$1
	if [[ ! -f "${FILE}" ]] ; then
		printf "File not found or not a regular file: %s\nAborting\n" "${FILE}" >&2
		exit 1
	fi

	dir=$(realpath $(dirname "${FILE}"))
	filename=$(basename "${FILE}")
	src="${dir}/${filename}"
	relpath=${dir#${HOME}}
	target="${DOTFILES_DIR}/${relpath}/${filename}"
	if [[ ! -d "${DOTFILES_DIR}/${relpath}" ]] ;then
		mkdir -p "${DOTFILES_DIR}/${relpath}"
	fi	

	if [[ -e "${target}" ]] ; then
		printf 'Target file exists already in dotfiles dir: `%s`.\nAborting\n' ${target}; >&2
		exit 1;
	fi

	cp "${src}" "${target}"
	mv "${src}" "${src}.bak"
	ln -s "${target}" "${src}"
	[ $QUIET ] || printf 'File has been imported and symlinked: `%s` -> `%s`\n' "${FILE}" "${target}"
	[ $QUIET ] || printf 'A backup of the original file has been written to `%s`\n' "${src}.bak"
}



do_install() {
	find ${DOTFILES_DIR} -type f  | while read file ; do
		DIR=$(dirname "${file}")
		DIR=${DIR##${DOTFILES_DIR}}
		FILENAME=$(basename "${file}")
		DESTDIR=${HOME}${DIR}

		if [ ! -e "${DESTDIR}" ] ; then
			[ $DRY_RUN ] || mkdir -p "${DESTDIR}"
		fi

		# Backup existing file, if present
		DESTFILE=${DESTDIR}/${FILENAME}
		if [ -e "${DESTFILE}" ] ; then 

			# echo "Backing up ${DESTFILE} to ${BACKUP_DIR}/${DIR}/${FILENAME}"

			if [ ! -e "${BACKUP_DIR}/${DIR}" ] ; then
				[ $DRY_RUN ] && echo "creating Directory: ${BACKUP_DIR}/${DIR}"
				[ $DRY_RUN ] || mkdir -p "${BACKUP_DIR}/${DIR}"
			fi
			# cp -L --> dereference in case of a symbolic link
			# copy the link target to a regular file
			[ $DRY_RUN ] && printf "[DRY RUN] Copying ${DESTFILE} to ${BACKUP_DIR}/${DIR}\n"
			[ $DRY_RUN ] || cp -L "${DESTFILE}" "${BACKUP_DIR}/${DIR}/"
			\rm "${DESTFILE}" # Optionally, seems a bit "cleaner"
		fi
		
		[ $QUIET ] || printf "Installing file: ${DESTFILE}\n"
		[ $DRY_RUN ] && printf "[DRY RUN] Create symlink from ${file} to ${DESTFILE}\n"
		[ $DRY_RUN ] || ln -sf --backup=numbered "${file}" "${DESTFILE}"
	done
}



eval "$(getoptions parser_definition) exit 1"


if [ $# -gt 0 ] ; then

	# Set default values for the directory paths
	[ -z "${DOTFILES_DIR}" ] && DOTFILES_DIR=${HOME}/dotfiles/files
	[ -z "${BACKUP_DIR}" ] && BACKUP_DIR=${HOME}/dotfiles/backup

	# [ $QUIET ] || printf -- "--------------------------------------------------------\n"
	# [ $QUIET ] || printf "DOTFILES_DIR = %s\n" ${DOTFILES_DIR}
	# [ $QUIET ] || printf "BACKUP_DIR   = %s\n" ${BACKUP_DIR}
	# [ $QUIET ] || printf -- "--------------------------------------------------------\n"

	# Assert dotfiles path exists and is a directory
	if [[ ! -d ${DOTFILES_DIR} ]] ; then
		printf "Dotfiles dir does not exist or is not a directory: \`${DOTFILES_DIR}\`\n" >&2
		exit 1
	fi

	cmd=$1
	shift

	case $cmd in
		"install")
			eval "$(getoptions parser_definition_install)"
			do_install
			;;
		"add")
			eval "$(getoptions parser_definition_add)"
			do_add $1
			;;
		--)
			help
	esac
else
	usage
fi


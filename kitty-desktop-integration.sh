#!/bin/bash
set -xe
# Better Kitty Icons
KITTY_DIR=$HOME/.local/kitty.app
KITTY_ICON_DIR=$HOME/Downloads/kitty-icon
# git clone https://github.com/k0nserv/kitty-icon.git $KITTY_ICON_DIR
# mv $KITTY_DIR/lib/kitty/logo/kitty.png{,orig} 
# mv $KITTY_DIR/lib/kitty/logo/kitty-128.png{,.orig}
cp $KITTY_ICON_DIR/build/neue_outrun.iconset/icon_32x32.png $KITTY_DIR/lib/kitty/logo/kitty.png
cp $KITTY_ICON_DIR/build/neue_outrun.iconset/icon_128x128.png $KITTY_DIR/lib/kitty/logo/kitty-128.png
sed -i "s/Icon=kitty/Icon=${KITTY_DIR}\/lib\/kitty\/logo\/kitty.png/" $KITTY_DIR/share/applications/kitty.desktop
# Create symbolic links to add kitty and kitten to PATH (assuming ~/.local/bin is in your system-wide PATH)
ln -sf $KITTY_DIR/bin/kitty $KITTY_DIR/bin/kitten ~/.local/bin/
# Place the kitty.desktop file somewhere it can be found by the OS
cp ~/.local/kitty.app/share/applications/kitty.desktop ~/.local/share/applications/
# If you want to open text files and images in kitty via your file manager also add the kitty-open.desktop file
cp ~/.local/kitty.app/share/applications/kitty-open.desktop ~/.local/share/applications/
# Update the paths to the kitty and its icon in the kitty desktop file(s)
sed -i "s|Icon=kitty|Icon=$HOME/.local/kitty.app/share/icons/hicolor/256x256/apps/kitty.png|g" ~/.local/share/applications/kitty*.desktop
sed -i "s|Exec=kitty|Exec=$HOME/.local/kitty.app/bin/kitty|g" ~/.local/share/applications/kitty*.desktop
# Make xdg-terminal-exec (and hence desktop environments that support it use kitty)
echo 'kitty.desktop' > ~/.config/xdg-terminals.list


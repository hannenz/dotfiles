// This file can be used with the themer CLI, see https://github.com/themerdev/themer

module.exports.colors = {
  "dark": {
    "shade0": "#3B4252",
    "shade7": "#ECEFF4",
    "accent0": "#BF616A",
    "accent1": "#D08770",
    "accent2": "#EBCB8B",
    "accent3": "#A3BE8C",
    "accent4": "#88C0D0",
    "accent5": "#5E81AC",
    "accent6": "#B48EAD",
    "accent7": "#B48EAD"
  }
};

// Your theme's URL: https://themer.dev/?colors.dark.shade0=%233b4252&colors.dark.shade7=%23eceff4&colors.dark.accent0=%23bf616a&colors.dark.accent1=%23d08770&colors.dark.accent2=%23ebcb8b&colors.dark.accent3=%23a3be8c&colors.dark.accent4=%2388c0d0&colors.dark.accent5=%235e81ac&colors.dark.accent6=%23b48ead&colors.dark.accent7=%23b48ead&activeColorSet=dark&calculateIntermediaryShades.dark=true&calculateIntermediaryShades.light=true
